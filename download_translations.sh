# Install jp:
# sudo apt-get install jq (Linux)
# brew install jq (Mac)
# chocolatey install jq (Windows)

# Install wget:
# brew install wget (Mac)

echo ''
echo 'Downloading translation files...'

config_file="download_translations_config.json"

token=$(jq .token ${config_file})
token="${token#\"}"
token="${token%\"}"
languages=$(jq .languages ${config_file})
languages="${languages#\"}"
languages="${languages%\"}"
folder=$(jq .folder ${config_file})
folder="${folder#\"}"
folder="${folder%\"}"

mkdir -p ${folder}

projects=$(jq .projects ${config_file})

for row in $(echo "${projects}" | jq -r '.[] | @base64'); do
    project_name=$(echo ${row} | base64 --decode | jq -r '.project_name')
    project_id=$(echo ${row} | base64 --decode | jq -r '.project_id')

    for language in $languages; do
        echo "Downloading -> $project_name [$project_id] ($language)"
        response=$(curl -sS --insecure -X POST https://api.poeditor.com/v2/projects/export \
            -d api_token="${token}" \
            -d id="${project_id}" \
            -d language="${language}" \
            -d type="yml")
        url=$(echo $response | jq -r '.result.url')
        wget -q -O "$folder/$project_name.$language.yml" "${url}"
    done
done

echo "$(date '+%Y-%m-%d %H:%M') - Poeditor command executed" >> "$folder/log.txt"

echo 'Translation file downloaded successfully.'
echo ''